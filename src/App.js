import React, {Component} from 'react';
import Routes from './routes';

import './App.css';

class App extends Component {
    constructor(){
        super();
        this.state={
            appName: "ReactJS Feed Example",
            home: false
        }
    }

    render() {
        return (
            <>
                <Routes name={this.state.appName}/>
            </>
        );
    }


}


export default App;

