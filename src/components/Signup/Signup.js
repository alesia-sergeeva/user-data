import React, {Component} from 'react';
import {PostData} from '../../services/PostData';
import {Redirect} from 'react-router-dom';
class Signup extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',


            redirectToReferrer: false
        };
        this.signup = this.signup.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    signup() {
        if(this.state.firstName && this.state.lastName && this.state.email && this.state.password){
            PostData('signup',this.state).then((result) => {
                let responseJson = result;
                if(responseJson.userData){
                    sessionStorage.setItem('userData',JSON.stringify(responseJson));
                    this.setState({redirectToReferrer: true});
                }
                else
                    alert(result.error);
            });
        }
    }
    onChange(e){
        this.setState({[e.target.name]:e.target.value});
    }
    render() {
        if (this.state.redirectToReferrer || sessionStorage.getItem('userData')) {
            return (<Redirect to={'/home'}/>)
        }
        return (
            <div className="row " id="sBody">
                <div className="medium-5 columns left">
                    <h4>Signup</h4>
                    <input type="text" name="firstName" placeholder="First Name" onChange={this.onChange}/>
                    <input type="text" name="lastName" placeholder="Last Name" onChange={this.onChange}/>
                    <input type="text" name="email" placeholder="Email" onChange={this.onChange}/>
                    <input type="password" name="password" placeholder="Password" onChange={this.onChange}/>
                    <input type="submit" className="button" value="Sign Up" onClick={this.signup}/>
                    <a href="/login">Login</a>
                </div>
            </div>
        );
    }
}
export default Signup;