import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import {PostData} from '../../services/PostData';
//import UserFeed from "../UserFeed/UserFeed";
import { confirmAlert } from 'react-confirm-alert';
//import '../../styles/react-confirm-alert.css';
import { Table, Button } from 'antd';

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
    },
    {
        title: 'ID',
        dataIndex: 'id',
    },
    {
        title: 'E-mail',
        dataIndex: 'email',
    },
    {
        title: 'Registration date',
        dataIndex: 'regDate',
    },
    {
        title: 'Last login date',
        dataIndex: 'lastLogDate',
    },
    {
        title: 'Status',
        dataIndex: 'status',
    }
];

const data = [];
for (let i = 0; i < 15; i++) {
    data.push({
        key: i,
        firstName: '',
        lastName: '',
        id: '',
        email: '',
        regDate: '12.05.2020',
        lastLogDate: '06.08.2020',
        status: 'Active'

    });
}

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            email: '',
            redirectToReferrer: false,
            firstName:'',
            lastName: '',
            selectedRowKeys: [], // Check here to configure the default column
            loading: false,
        };
        //this.feedUpdate = this.feedUpdate.bind(this);
        //this.deleteFeedAction = this.deleteFeedAction.bind(this);
        this.logout = this.logout.bind(this);
    }
    componentWillMount() {
        if(sessionStorage.getItem("userData")){
            this.getUserFeed();
        }
        else{
            this.setState({redirectToReferrer: true});
        }
    }
    start = () => {
        this.setState({loading: true});
        // ajax request after empty completing
        setTimeout(() => {
            this.setState({
                selectedRowKeys: [],
                loading: false,
            });
        }, 1000);
    };

    onSelectChange = selectedRowKeys => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({selectedRowKeys});
    };


    feedUpdate(e) {
        e.preventDefault();
        let data = JSON.parse(sessionStorage.getItem("userData"));
        let postData = { user_id: data.userData.user_id, feed: this.state.userFeed };
        if (this.state.userFeed) {
            PostData('feedUpdate', postData).then((result) => {
                let responseJson = result;
                this.setState({data: responseJson.feedData});
            });
        }
    }


    deleteFeedAction(e){
        let updateIndex=e.target.getAttribute('value');
        let feed_id=document.getElementById("del").getAttribute("data");
        let data = JSON.parse(sessionStorage.getItem("userData"));
        let postData = { user_id: data.userData.user_id,feed_id: feed_id };
        if (postData) {
            PostData('feedDelete', postData).then((result) => {
                this.state.data.splice(updateIndex,1);
                this.setState({data:this.state.data});
                if(result.success){
                    alert(result.success);
                }
                else
                    alert(result.error);
            });
        }
    }

    logout(){
        sessionStorage.setItem("userData",'');
        sessionStorage.clear();
        this.setState({redirectToReferrer: true});
    }

    render() {
        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/login'}/>)
        }
        const {loading, selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        }
        const hasSelected = selectedRowKeys.length > 0;
        return (
            <>
                <div style={{ marginBottom: 16, padding: 8 }}>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                        Block
                    </Button>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                        Unblock
                    </Button>
                    <Button type="primary" onClick={this.start} disabled={!hasSelected} loading={loading}>
                        Delete
                    </Button>
                    <span style={{ marginLeft: 8 }}>
                {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
                </span>
                </div>
                <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
                <a href="#" onClick={this.logout} className="logout">Logout</a>
                <form onSubmit={this.feedUpdate} method="post">
                    <input name="userFeed" onChange={this.onChange} value={this.state.userFeed} type="text" placeholder="Write your feed here..."/>
                    <input type="submit" value="Post" className="button" onClick={this.feedUpdate}/>
                </form>
            </>
        );
    }
}
export default HomePage;